package HeadBoyElection;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.event.*;

import headboyLogic.CreateNewSchueler;

/**
  *
  * Beschreibung
  *
  * @version 1.0 vom 06.11.2018
  * @author 
  */

public class Registrierung extends JFrame {
  // Anfang Attribute 
  private JPanel jPanel1 = new JPanel(null, true);
    private JLabel lRegistrierung = new JLabel();
    private JLabel lVorname = new JLabel();
    private JLabel lNachname = new JLabel();
    private JLabel lPasswort = new JLabel();
    private JLabel lPasswortbestatigen = new JLabel();
    private JLabel lJahrgang = new JLabel();
    private JTextField txtVorname = new JTextField();
    private JTextField txtNachname = new JTextField();
    private JPasswordField pwfPasswort = new JPasswordField();
    private JPasswordField pwfPasswortb = new JPasswordField();
    private JTextField txtJahrgang = new JTextField();
    private JButton btnZurueck = new JButton();
    private JButton bRegistrieren = new JButton();
  // Ende Attribute
  
  public Registrierung() { 
    // Frame-Initialisierung
    super();
    setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
    int frameWidth = 600; 
    int frameHeight = 400;
    setSize(frameWidth, frameHeight);
    Dimension d = Toolkit.getDefaultToolkit().getScreenSize();
    int x = (d.width - getSize().width) / 2;
    int y = (d.height - getSize().height) / 2;
    setLocation(x, y);
    setTitle("Registrierung");
    setResizable(false);
    Container cp = getContentPane();
    cp.setLayout(null);
    // Anfang Komponenten
    
    jPanel1.setBounds(0, -16, 600, 400);
    jPanel1.setOpaque(false);
    cp.add(jPanel1);
    lRegistrierung.setBounds(168, 24, 238, 52);
    lRegistrierung.setText("Registrierung");
    lRegistrierung.setHorizontalAlignment(SwingConstants.CENTER);
    jPanel1.add(lRegistrierung);
    lVorname.setBounds(240, 88, 110, 20);
    lVorname.setText("Vorname");
    jPanel1.add(lVorname);
    lNachname.setBounds(240, 128, 110, 20);
    lNachname.setText("Nachname");
    jPanel1.add(lNachname);
    lPasswort.setBounds(240, 168, 110, 20);
    lPasswort.setText("Passwort");
    jPanel1.add(lPasswort);
    lPasswortbestatigen.setBounds(240, 208, 122, 20);
    lPasswortbestatigen.setText("Passwort best�tigen");
    jPanel1.add(lPasswortbestatigen);
    lJahrgang.setBounds(240, 248, 110, 20);
    lJahrgang.setText("Jahrgang");
    jPanel1.add(lJahrgang);
    txtVorname.setBounds(240, 104, 150, 20);
    jPanel1.add(txtVorname);
    txtNachname.setBounds(240, 144, 150, 20);
    jPanel1.add(txtNachname);
    pwfPasswort.setBounds(240, 184, 150, 20);
    jPanel1.add(pwfPasswort);
    pwfPasswortb.setBounds(240, 224, 150, 20);
    jPanel1.add(pwfPasswortb);
    txtJahrgang.setBounds(240, 264, 150, 20);
    jPanel1.add(txtJahrgang);
    btnZurueck.setBounds(64, 40, 75, 25);
    btnZurueck.setText("Zur�ck");
    btnZurueck.setMargin(new Insets(2, 2, 2, 2));
    btnZurueck.addActionListener(new ActionListener() { 
     public void actionPerformed(ActionEvent evt) { 
        btnZurueck_ActionPerformed(evt);
      }
    });
    jPanel1.add(btnZurueck);
    bRegistrieren.setBounds(448, 328, 99, 25);
    bRegistrieren.setText("Registrieren");
    bRegistrieren.setMargin(new Insets(2, 2, 2, 2));
    bRegistrieren.addActionListener(new ActionListener() { 
      public void actionPerformed(ActionEvent evt) { 
        bRegistrieren_ActionPerformed(evt);
      }
    });
    jPanel1.add(bRegistrieren);
    // Ende Komponenten
    
    setVisible(true);
  } // end of public Registrierung
  
  // Anfang Methoden
  
  public static void main(String[] args) {
    new Registrierung();
  } // end of main
  
  public void btnZurueck_ActionPerformed(ActionEvent evt) {
    AdministratorFenster adminfenster = new AdministratorFenster();
    this.dispose();
    
  } // end of btnZurueck_ActionPerformed

  public void bRegistrieren_ActionPerformed(ActionEvent evt) {
    if (pwfPasswort.getText()==pwfPasswortb.getText()) {
    	CreateNewSchueler createNewSchueler = new CreateNewSchueler();
        createNewSchueler.createNewSchueler(txtVorname.getText(),txtNachname.getText(),pwfPasswort.getText(),txtJahrgang.getText());
        txtVorname.setText("");
        txtNachname.setText("");
        pwfPasswort.setText("");
        txtJahrgang.setText("");
    }else {
    	Fehlermeldung fehler = new Fehlermeldung();
    	pwfPasswortb.setBackground(Color.RED);
    }
    
  } // end of bRegistrieren_ActionPerformed

  // Ende Methoden
} // end of class Registrierung
