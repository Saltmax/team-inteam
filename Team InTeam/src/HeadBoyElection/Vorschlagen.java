package HeadBoyElection;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.event.*;

import headboyLogic.Schueler;

/**
  *
  * Beschreibung
  *
  * @version 1.0 vom 06.11.2018
  * @author 
  */

public class Vorschlagen extends JFrame {    
  // Anfang Attribute
  private JPanel jPanel1 = new JPanel(null, true);
    private JLabel lVorschlagen = new JLabel();
    private JLabel lVorname = new JLabel();
    private JLabel lNachname = new JLabel();
    private JLabel lJahrgang = new JLabel();
    private JTextField jTextField1 = new JTextField();
    private JTextField jTextField2 = new JTextField();
    private JTextField jTextField3 = new JTextField();
    private JButton bVorschlagen = new JButton();
    private JButton btnZurueck = new JButton();
  
    // Ende Attribute
  
  public Vorschlagen() { 
    // Frame-Initialisierung
    super();
    setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
    int frameWidth = 600; 
    int frameHeight = 400;
    setSize(frameWidth, frameHeight);
    Dimension d = Toolkit.getDefaultToolkit().getScreenSize();
    int x = (d.width - getSize().width) / 2;
    int y = (d.height - getSize().height) / 2;
    setLocation(x, y);
    setTitle("Vorschlagen");
    setResizable(false);
    Container cp = getContentPane();
    cp.setLayout(null);
    // Anfang Komponenten
    
    jPanel1.setBounds(0, 0, 600, 400);
    jPanel1.setOpaque(false);
    cp.add(jPanel1);
    lVorschlagen.setBounds(200, 0, 182, 44);
    lVorschlagen.setText("Vorschlagen");
    lVorschlagen.setHorizontalAlignment(SwingConstants.CENTER);
    jPanel1.add(lVorschlagen);
    lVorname.setBounds(144, 96, 110, 20);
    lVorname.setText("Vorname");
    jPanel1.add(lVorname);
    lNachname.setBounds(144, 144, 110, 20);
    lNachname.setText("Nachname");
    jPanel1.add(lNachname);
    lJahrgang.setBounds(144, 192, 110, 20);
    lJahrgang.setText("Jahrgang");
    jPanel1.add(lJahrgang);
    jTextField1.setBounds(144, 112, 150, 20);
    jPanel1.add(jTextField1);
    jTextField2.setBounds(144, 160, 150, 20);
    jPanel1.add(jTextField2);
    jTextField3.setBounds(144, 208, 150, 20);
    jPanel1.add(jTextField3);
    bVorschlagen.setBounds(368, 160, 91, 25);
    bVorschlagen.setText("Vorschlagen");
    bVorschlagen.setMargin(new Insets(2, 2, 2, 2));
    bVorschlagen.addActionListener(new ActionListener() { 
      public void actionPerformed(ActionEvent evt) { 
        bVorschlagen_ActionPerformed(evt);
      }
    });
    jPanel1.add(bVorschlagen);
    btnZurueck.setBounds(48, 8, 75, 25);
    btnZurueck.setText("Zur�ck");
    btnZurueck.setMargin(new Insets(2, 2, 2, 2));
    btnZurueck.addActionListener(new ActionListener() { 
      public void actionPerformed(ActionEvent evt) { 
        btnZurueck_ActionPerformed(evt);
      }
    });
    jPanel1.add(btnZurueck);
    // Ende Komponenten
    
    setVisible(true);
  } // end of public Vorschlagen
  
  // Anfang Methoden
  
  public static void main(String[] args) {
    new Vorschlagen();
  } // end of main
  
  public void bVorschlagen_ActionPerformed(ActionEvent evt) {
    Schueler schueler = new Schueler ();
    schueler.vorschlagen(jTextField1.getText(),jTextField2.getText(),jTextField3.getText());
    jTextField1.setText("");
    jTextField2.setText("");
    jTextField3.setText("");
    
  } // end of bVorschlagen_ActionPerformed

  public void btnZurueck_ActionPerformed(ActionEvent evt) {
    Hauptmenue hauptmenue = new Hauptmenue ();
    this.dispose();
    
  } // end of btnZurueck_ActionPerformed

  // Ende Methoden
} // end of class Vorschlagen
