package HeadBoyElection;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.event.*;

/**
  *
  * Beschreibung
  *
  * @version 1.0 vom 06.11.2018
  * @author 
  */

public class Amtaus�ben extends JFrame {
  // Anfang Attribute
  private JPanel jPanel1 = new JPanel(null, true);
    private JButton btnJa = new JButton();
    private JButton btnNein = new JButton();
    private JLabel lblSiehabengenugStimmenerhalten = new JLabel();
    private JLabel lblMochtenSiedasAmtausuben = new JLabel();
  // Ende Attribute
  
  public Amtaus�ben() { 
    // Frame-Initialisierung
    super();
    setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
    int frameWidth = 300; 
    int frameHeight = 200;
    setSize(frameWidth, frameHeight);
    Dimension d = Toolkit.getDefaultToolkit().getScreenSize();
    int x = (d.width - getSize().width) / 2;
    int y = (d.height - getSize().height) / 2;
    setLocation(x, y);
    setTitle("Amtaus�ben");
    setResizable(false);
    Container cp = getContentPane();
    cp.setLayout(null);
    // Anfang Komponenten
    
    jPanel1.setBounds(0, 0, 300, 200);
    jPanel1.setOpaque(false);
    cp.add(jPanel1);
    btnJa.setBounds(48, 96, 75, 25);
    btnJa.setText("Ja");
    btnJa.setMargin(new Insets(2, 2, 2, 2));
    btnJa.addActionListener(new ActionListener() { 
      public void actionPerformed(ActionEvent evt) { 
        btnJa_ActionPerformed(evt);
      }
    });
    jPanel1.add(btnJa);
    btnNein.setBounds(160, 96, 75, 25);
    btnNein.setText("Nein");
    btnNein.setMargin(new Insets(2, 2, 2, 2));
    btnNein.addActionListener(new ActionListener() { 
      public void actionPerformed(ActionEvent evt) { 
        btnNein_ActionPerformed(evt);
      }
    });
    jPanel1.add(btnNein);
    lblSiehabengenugStimmenerhalten.setBounds(40, 24, 205, 20);
    lblSiehabengenugStimmenerhalten.setText("Sie haben genug Stimmen erhalten!");
    jPanel1.add(lblSiehabengenugStimmenerhalten);
    lblMochtenSiedasAmtausuben.setBounds(56, 48, 186, 20);
    lblMochtenSiedasAmtausuben.setText("M�chten Sie das Amt aus�ben? ");
    jPanel1.add(lblMochtenSiedasAmtausuben);
    // Ende Komponenten
    
    setVisible(true);
  } // end of public Amtaus�ben
  
  // Anfang Methoden
  
  public static void main(String[] args) {
    new Amtaus�ben();
  } // end of main
  
  public void btnJa_ActionPerformed(ActionEvent evt) {
    // TODO hier Quelltext einf�gen
    
  } // end of btnJa_ActionPerformed

  public void btnNein_ActionPerformed(ActionEvent evt) {
    // TODO hier Quelltext einf�gen
    
  } // end of btnNein_ActionPerformed

  // Ende Methoden
} // end of class Amtaus�ben
