package HeadBoyElection;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.event.*;

/**
  *
  * Beschreibung
  *
  * @version 1.0 vom 06.11.2018
  * @author 
  */

public class Bereitsvorgeschlagen extends JFrame {
  // Anfang Attribute
  private JPanel jPanel1 = new JPanel(null, true);
    private JButton btnOK = new JButton();
    private JLabel lblDieserUserwurdebereitsvorgeschlagen = new JLabel();
  // Ende Attribute
  
  public Bereitsvorgeschlagen() { 
    // Frame-Initialisierung
    super();
    setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
    int frameWidth = 300; 
    int frameHeight = 200;
    setSize(frameWidth, frameHeight);
    Dimension d = Toolkit.getDefaultToolkit().getScreenSize();
    int x = (d.width - getSize().width) / 2;
    int y = (d.height - getSize().height) / 2;
    setLocation(x, y);
    setTitle("Bereitsvorgeschlagen");
    setResizable(false);
    Container cp = getContentPane();
    cp.setLayout(null);
    // Anfang Komponenten
    
    jPanel1.setBounds(0, 0, 300, 200);
    jPanel1.setOpaque(false);
    cp.add(jPanel1);
    btnOK.setBounds(112, 96, 75, 25);
    btnOK.setText("OK");
    btnOK.setMargin(new Insets(2, 2, 2, 2));
    btnOK.addActionListener(new ActionListener() { 
      public void actionPerformed(ActionEvent evt) { 
        btnOK_ActionPerformed(evt);
      }
    });
    jPanel1.add(btnOK);
    lblDieserUserwurdebereitsvorgeschlagen.setBounds(24, 56, 243, 20);
    lblDieserUserwurdebereitsvorgeschlagen.setText("Dieser User wurde bereits vorgeschlagen!");
    jPanel1.add(lblDieserUserwurdebereitsvorgeschlagen);
    // Ende Komponenten
    
    setVisible(true);
  } // end of public Bereitsvorgeschlagen
  
  // Anfang Methoden
  
  public static void main(String[] args) {
    new Bereitsvorgeschlagen();
  } // end of main
  
  public void btnOK_ActionPerformed(ActionEvent evt) {
    this.dispose();
    
  } // end of btnOK_ActionPerformed

  // Ende Methoden
} // end of class Bereitsvorgeschlagen

