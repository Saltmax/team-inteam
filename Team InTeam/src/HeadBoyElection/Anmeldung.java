package HeadBoyElection;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.event.*;

import headboyLogic.Login;

/**
  *
  * Beschreibung
  *
  * @version 1.0 vom 06.11.2018
  * @author 
  */

public class Anmeldung extends JFrame {
  // Anfang Attribute           
  private JPanel jPanel1 = new JPanel(null, true);
    private JLabel lblAnmeldung = new JLabel();
    private JButton btnZurueck = new JButton();
    private JButton bBestatigen = new JButton();
    private JLabel lBenutzername = new JLabel();
    private JLabel lPasswort = new JLabel();
    private JTextField txtBenutzername = new JTextField();
    private JPasswordField pwfPasswort = new JPasswordField();
   Login login = new Login();

   // Ende Attribute
  
  public Anmeldung() { 
    // Frame-Initialisierung
    super();
    setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
    int frameWidth = 600; 
    int frameHeight = 400;
    setSize(frameWidth, frameHeight);
    Dimension d = Toolkit.getDefaultToolkit().getScreenSize();
    int x = (d.width - getSize().width) / 2;
    int y = (d.height - getSize().height) / 2;
    setLocation(x, y);
    setTitle("Anmeldung");
    setResizable(false);
    Container cp = getContentPane();
    cp.setLayout(null);
    // Anfang Komponenten
    
    jPanel1.setBounds(0, 0, 600, 400);
    jPanel1.setOpaque(false);
    cp.add(jPanel1);
    lblAnmeldung.setBounds(152, 8, 262, 36);
    lblAnmeldung.setText("Anmeldung");
    lblAnmeldung.setHorizontalAlignment(SwingConstants.CENTER);
    jPanel1.add(lblAnmeldung);
    btnZurueck.setBounds(40, 16, 75, 25);
    btnZurueck.setText("Zur�ck");
    btnZurueck.setMargin(new Insets(2, 2, 2, 2));
    btnZurueck.addActionListener(new ActionListener() { 
      public void actionPerformed(ActionEvent evt) { 
        btnZurueck_ActionPerformed(evt);
      }
    });
    jPanel1.add(btnZurueck);
    bBestatigen.setBounds(488, 304, 75, 25);
    bBestatigen.setText("Best�tigen");
    bBestatigen.setMargin(new Insets(2, 2, 2, 2));
    bBestatigen.addActionListener(new ActionListener() { 
      public void actionPerformed(ActionEvent evt) { 
        bBestatigen_ActionPerformed(evt);
      }
    });
    jPanel1.add(bBestatigen);
    lBenutzername.setBounds(224, 112, 110, 20);
    lBenutzername.setText("Benutzername");
    jPanel1.add(lBenutzername);
    lPasswort.setBounds(224, 200, 110, 20);
    lPasswort.setText("Passwort");
    jPanel1.add(lPasswort);
    txtBenutzername.setBounds(224, 128, 150, 20);
    jPanel1.add(txtBenutzername);
    pwfPasswort.setBounds(224, 216, 150, 20);
    jPanel1.add(pwfPasswort);
    // Ende Komponenten
    
    setVisible(true);
  } // end of public Anmeldung
  
  // Anfang Methoden
  
  public static void main(String[] args) {
    new Anmeldung();
  } // end of main
  
  public void btnZurueck_ActionPerformed(ActionEvent evt) {
    Startbildschirm startbildschirm = new Startbildschirm();
    this.dispose();
    
  } // end of btnZurueck_ActionPerformed

  public void bBestatigen_ActionPerformed(ActionEvent evt) {
	  login.logIn(txtBenutzername.getSelectedText(),pwfPasswort.getSelectedText());
	  this.dispose();
    
  } // end of bBestatigen_ActionPerformed

  // Ende Methoden
} // end of class Anmeldung
