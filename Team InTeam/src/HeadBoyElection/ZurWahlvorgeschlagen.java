package HeadBoyElection;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.event.*;

import HeadBoyDBSchicht.Datenbankschicht;
import headboyLogic.CreateNewSchueler;
import headboyLogic.Kandidat;
import headboyLogic.Login;
import headboyLogic.Schueler;

/**
  *
  * Beschreibung
  *
  * @version 1.0 vom 14.11.2018
  * @author 
  */

public class ZurWahlvorgeschlagen extends JFrame {
  // Anfang Attribute
  private JPanel jPanel1 = new JPanel(null, true);
    private JButton btnJa1 = new JButton();
    private JButton btnNein = new JButton();
    private JLabel lSiewurdenzurWahlvorgeschlagen = new JLabel();
    private JLabel lblMochtenSiedieseantreten1 = new JLabel();
  // Ende Attribute
  
  public ZurWahlvorgeschlagen() { 
    // Frame-Initialisierung
    super();
    setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
    int frameWidth = 300; 
    int frameHeight = 200;
    setSize(frameWidth, frameHeight);
    Dimension d = Toolkit.getDefaultToolkit().getScreenSize();
    int x = (d.width - getSize().width) / 2;
    int y = (d.height - getSize().height) / 2;
    setLocation(x, y);
    setTitle("ZurWahlvorgeschlagen");
    setResizable(false);
    Container cp = getContentPane();
    cp.setLayout(null);
    // Anfang Komponenten
    
    jPanel1.setBounds(0, 0, 300, 200);
    jPanel1.setOpaque(false);
    cp.add(jPanel1);
    btnJa1.setBounds(48, 112, 75, 25);
    btnJa1.setText("Ja");
    btnJa1.setMargin(new Insets(2, 2, 2, 2));
    btnJa1.addActionListener(new ActionListener() { 
      public void actionPerformed(ActionEvent evt) { 
        btnJa1_ActionPerformed(evt);
      }
    });
    jPanel1.add(btnJa1);
    btnNein.setBounds(176, 112, 75, 25);
    btnNein.setText("Nein");
    btnNein.setMargin(new Insets(2, 2, 2, 2));
    btnNein.addActionListener(new ActionListener() { 
      public void actionPerformed(ActionEvent evt) { 
        btnNein_ActionPerformed(evt);
      }
    });
    jPanel1.add(btnNein);
    lSiewurdenzurWahlvorgeschlagen.setBounds(56, 48, 211, 20);
    lSiewurdenzurWahlvorgeschlagen.setText("Sie wurden zur Wahl vorgeschlagen,");
    jPanel1.add(lSiewurdenzurWahlvorgeschlagen);
    lblMochtenSiedieseantreten1.setBounds(72, 64, 173, 20);
    lblMochtenSiedieseantreten1.setText("m�chten Sie diese antreten? ");
    jPanel1.add(lblMochtenSiedieseantreten1);
    // Ende Komponenten
    
    setVisible(true);
  } // end of public ZurWahlvorgeschlagen
  
  // Anfang Methoden
  
  public static void main(String[] args) {
    new ZurWahlvorgeschlagen();
  } // end of main
  
  public void btnJa1_ActionPerformed(ActionEvent evt) {
	  Datenbankschicht dB = new Datenbankschicht();
	  Schueler dude = new Schueler ();
	  Login login = new Login ();
	  dude = dude.getSchueler(login.getAktuellerUser());
	  dB.schuelerLoeschen(dude);
	  Kandidat kandidat = new Kandidat ();
	  kandidat.kandidatEintragen(dude);
	  this.dispose();
    
  } // end of btnJa1_ActionPerformed

  public void btnNein_ActionPerformed(ActionEvent evt) {
  Schueler schueler = new Schueler ();
  schueler.setVorgeschlagen(false);
  } // end of btnNein_ActionPerformed

  // Ende Methoden
} // end of class ZurWahlvorgeschlagen
