package HeadBoyElection;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.event.*;

/**
  *
  * Beschreibung
  *
  * @version 1.0 vom 21.11.2018
  * @author 
  */

public class Waehlen extends JFrame {
  // Anfang Attribute
  private JPanel jPanel1 = new JPanel(null, true);
    private JLabel lWahlen = new JLabel();
    private JButton btnZurueck = new JButton();
    private JLabel lNamedesKandidaten = new JLabel();
    private JLabel lLeer = new JLabel();
    private JLabel lWollenSiediesemKandidaten = new JLabel();
    private JLabel lIhreStimmegeben = new JLabel();
    private JButton bJa = new JButton();
    private JButton bNein = new JButton();
    private JLabel lVerfugbareStimmen = new JLabel();
    private JTextField nf3 = new JTextField();
    private JLabel lZuwahlendeKandidaten = new JLabel();
    private JLabel l1 = new JLabel();
    private JLabel l2 = new JLabel();
    private JLabel l3 = new JLabel();
    private JLabel l4 = new JLabel();
    private JLabel l5 = new JLabel();
    private JLabel l6 = new JLabel();
    private JLabel l7 = new JLabel();
    private JLabel l8 = new JLabel();
    private JLabel l9 = new JLabel();
    private JLabel l10 = new JLabel();
    private JLabel l11 = new JLabel();
    private JLabel l12 = new JLabel();
  // Ende Attribute
  
  public Waehlen() { 
    // Frame-Initialisierung
    super();
    setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
    int frameWidth = 600; 
    int frameHeight = 400;
    setSize(frameWidth, frameHeight);
    Dimension d = Toolkit.getDefaultToolkit().getScreenSize();
    int x = (d.width - getSize().width) / 2;
    int y = (d.height - getSize().height) / 2;
    setLocation(x, y);
    setTitle("Waehlen");
    setResizable(false);
    Container cp = getContentPane();
    cp.setLayout(null);
    // Anfang Komponenten
    
    jPanel1.setBounds(0, 0, 600, 400);
    jPanel1.setOpaque(false);
    cp.add(jPanel1);
    lWahlen.setBounds(224, 0, 118, 44);
    lWahlen.setText("W�hlen");
    lWahlen.setHorizontalAlignment(SwingConstants.CENTER);
    jPanel1.add(lWahlen);
    btnZurueck.setBounds(40, 16, 75, 25);
    btnZurueck.setText("Zur�ck");
    btnZurueck.setMargin(new Insets(2, 2, 2, 2));
    btnZurueck.addActionListener(new ActionListener() { 
      public void actionPerformed(ActionEvent evt) { 
        btnZurueck_ActionPerformed(evt);
      }
    });
    jPanel1.add(btnZurueck);
    lNamedesKandidaten.setBounds(376, 72, 131, 20);
    lNamedesKandidaten.setText("Name des Kandidaten:");
    jPanel1.add(lNamedesKandidaten);
    lLeer.setBounds(384, 112, 118, 28);
    lLeer.setText("leer");
    lLeer.setHorizontalAlignment(SwingConstants.CENTER);
    jPanel1.add(lLeer);
    lWollenSiediesemKandidaten.setBounds(352, 192, 176, 20);
    lWollenSiediesemKandidaten.setText("Wollen Sie diesem Kandidaten");
    lWollenSiediesemKandidaten.setHorizontalAlignment(SwingConstants.CENTER);
    jPanel1.add(lWollenSiediesemKandidaten);
    lIhreStimmegeben.setBounds(384, 208, 118, 20);
    lIhreStimmegeben.setText("Ihre Stimme geben?");
    jPanel1.add(lIhreStimmegeben);
    bJa.setBounds(344, 264, 75, 25);
    bJa.setText("Ja");
    bJa.setMargin(new Insets(2, 2, 2, 2));
    bJa.addActionListener(new ActionListener() { 
      public void actionPerformed(ActionEvent evt) { 
        bJa_ActionPerformed(evt);
      }
    });
    jPanel1.add(bJa);
    bNein.setBounds(464, 264, 75, 25);
    bNein.setText("Nein");
    bNein.setMargin(new Insets(2, 2, 2, 2));
    bNein.addActionListener(new ActionListener() { 
      public void actionPerformed(ActionEvent evt) { 
        bNein_ActionPerformed(evt);
      }
    });
    jPanel1.add(bNein);
    lVerfugbareStimmen.setBounds(344, 304, 127, 20);
    lVerfugbareStimmen.setText("Verf�gbare Stimmen:");
    jPanel1.add(lVerfugbareStimmen);
    nf3.setBounds(472, 304, 27, 20);
    nf3.setText("3");
    nf3.setHorizontalAlignment(SwingConstants.CENTER);
    jPanel1.add(nf3);
    lZuwahlendeKandidaten.setBounds(64, 80, 146, 20);
    lZuwahlendeKandidaten.setText("Zu w�hlende Kandidaten:");
    lZuwahlendeKandidaten.setHorizontalAlignment(SwingConstants.CENTER);
    jPanel1.add(lZuwahlendeKandidaten);
    l1.setBounds(64, 112, 110, 20);
    l1.setText("1.");
    jPanel1.add(l1);
    l2.setBounds(64, 128, 110, 20);
    l2.setText("2.");
    jPanel1.add(l2);
    l3.setBounds(64, 144, 110, 20);
    l3.setText("3.");
    jPanel1.add(l3);
    l4.setBounds(64, 160, 110, 20);
    l4.setText("4.");
    jPanel1.add(l4);
    l5.setBounds(64, 176, 110, 20);
    l5.setText("5.");
    jPanel1.add(l5);
    l6.setBounds(64, 192, 110, 20);
    l6.setText("6.");
    jPanel1.add(l6);
    l7.setBounds(64, 208, 110, 20);
    l7.setText("7.");
    jPanel1.add(l7);
    l8.setBounds(64, 224, 110, 20);
    l8.setText("8.");
    jPanel1.add(l8);
    l9.setBounds(64, 240, 110, 20);
    l9.setText("9.");
    jPanel1.add(l9);
    l10.setBounds(64, 256, 110, 20);
    l10.setText("10.");
    jPanel1.add(l10);
    l11.setBounds(64, 272, 110, 20);
    l11.setText("11.");
    jPanel1.add(l11);
    l12.setBounds(64, 288, 110, 20);
    l12.setText("12.");
    jPanel1.add(l12);
    // Ende Komponenten
    
    setVisible(true);
  } // end of public Waehlen
  
  // Anfang Methoden
  
  public static void main(String[] args) {
    new Waehlen();
  } // end of main
  
  public void bJa_ActionPerformed(ActionEvent evt) {


	
	  
    
  } // end of bJa_ActionPerformed

  public void bNein_ActionPerformed(ActionEvent evt) {
    // TODO hier Quelltext einf�gen	
    
  } // end of bNein_ActionPerformed

  public void btnZurueck_ActionPerformed(ActionEvent evt) {
    Hauptmenue hauptmenue = new Hauptmenue ();
    this.dispose();
    
  } // end of btnZurueck_ActionPerformed

  // Ende Methoden
} // end of class Waehlen
