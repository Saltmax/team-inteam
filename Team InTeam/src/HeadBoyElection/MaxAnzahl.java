package HeadBoyElection;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.event.*;

/**
  *
  * Beschreibung
  *
  * @version 1.0 vom 20.11.2018
  * @author 
  */

public class MaxAnzahl extends JFrame {
  // Anfang Attribute
  private JPanel jPanel1 = new JPanel(null, true);
    private JButton btnOK = new JButton();
    private JLabel lDiemaximaleAnzahlisterreicht1 = new JLabel();
  // Ende Attribute
  
  public MaxAnzahl() { 
    // Frame-Initialisierung
    super();
    setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
    int frameWidth = 300; 
    int frameHeight = 200;
    setSize(frameWidth, frameHeight);
    Dimension d = Toolkit.getDefaultToolkit().getScreenSize();
    int x = (d.width - getSize().width) / 2;
    int y = (d.height - getSize().height) / 2;
    setLocation(x, y);
    setTitle("MaxAnzahl");
    setResizable(false);
    Container cp = getContentPane();
    cp.setLayout(null);
    // Anfang Komponenten
    
    jPanel1.setBounds(0, 0, 300, 200);
    jPanel1.setOpaque(false);
    cp.add(jPanel1);
    btnOK.setBounds(112, 96, 75, 25);
    btnOK.setText("OK");
    btnOK.setMargin(new Insets(2, 2, 2, 2));
    btnOK.addActionListener(new ActionListener() { 
      public void actionPerformed(ActionEvent evt) { 
        btnOK_ActionPerformed(evt);
      }
    });
    jPanel1.add(btnOK);
    lDiemaximaleAnzahlisterreicht1.setBounds(56, 48, 193, 20);
    lDiemaximaleAnzahlisterreicht1.setText("Die maximale Anzahl ist erreicht! ");
    jPanel1.add(lDiemaximaleAnzahlisterreicht1);
    // Ende Komponenten
    
    setVisible(true);
  } // end of public MaxAnzahl
  
  // Anfang Methoden
  
  public static void main(String[] args) {
    new MaxAnzahl();
  } // end of main
  
  public void btnOK_ActionPerformed(ActionEvent evt) {
    // TODO hier Quelltext einf�gen
    
  } // end of btnOK_ActionPerformed

  // Ende Methoden
} // end of class MaxAnzahl
