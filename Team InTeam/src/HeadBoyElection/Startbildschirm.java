 package HeadBoyElection;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.event.*;

import HeadBoyDBSchicht.Datenbankschicht;
import headboyLogic.CreateNewAdmin;
import headboyLogic.Registrieren;

/**
  *
  * Beschreibung
  *                                                                                               
  * @version 1.0 vom 06.11.2018                                                              
  * @author 
  */

public class Startbildschirm extends JFrame {
  // Anfang Attribute                                       
  private JPanel jPanel1 = new JPanel(null, true);
    private JLabel lHeadBoyElection = new JLabel();
    private JButton bAnmelden = new JButton();
    private JButton btnRegistrieren = new JButton();
  // Ende Attribute
  
  public Startbildschirm() { 
    // Frame-Initialisierung
    super();
    setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
    int frameWidth = 600; 
    int frameHeight = 400;
    setSize(frameWidth, frameHeight);
    Dimension d = Toolkit.getDefaultToolkit().getScreenSize();
    int x = (d.width - getSize().width) / 2;
    int y = (d.height - getSize().height) / 2;
    setLocation(x, y);
    setTitle("Test");
    setResizable(false);
    Container cp = getContentPane();
    cp.setLayout(null);
    // Anfang Komponenten
    
    jPanel1.setBounds(0, 0, 600, 400);
    jPanel1.setOpaque(false);
    cp.add(jPanel1);
    lHeadBoyElection.setBounds(174, 0, 228, 50);
    lHeadBoyElection.setText("Head Boy Election");
    lHeadBoyElection.setHorizontalTextPosition(SwingConstants.CENTER);
    lHeadBoyElection.setHorizontalAlignment(SwingConstants.CENTER);
    jPanel1.add(lHeadBoyElection);
    bAnmelden.setBounds(208, 152, 163, 41);
    bAnmelden.setText("Anmelden");
    bAnmelden.setMargin(new Insets(2, 2, 2, 2));
    bAnmelden.addActionListener(new ActionListener() { 
      public void actionPerformed(ActionEvent evt) { 
        btnAnmelden_ActionPerformed(evt);
      }
    });
    jPanel1.add(bAnmelden);
   /* btnRegistrieren.setBounds(208, 222, 163, 41);
    btnRegistrieren.setText("Registrieren");
    btnRegistrieren.setMargin(new Insets(2, 2, 2, 2));
    btnRegistrieren.addActionListener(new ActionListener() { 
      public void actionPerformed(ActionEvent evt) { 
        btnRegistrieren_ActionPerformed(evt);
      }
    });
    jPanel1.add(btnRegistrieren);	*/
    // Ende Komponenten
    
    setVisible(true);
  } // end of public Test
  
  // Anfang Methoden
  
  public static void main(String[] args) {
    new Startbildschirm();
  } // end of main
  
  public void btnAnmelden_ActionPerformed(ActionEvent evt) {
	 Datenbankschicht dB = new Datenbankschicht ();
	  if (dB.istAdminVergeben()==false) {
		  CreateNewAdmin cna = new CreateNewAdmin();
		  cna.createNewAdmin();
	  }
	  Anmeldung anmelden = new Anmeldung ();
	  this.dispose();
  } // end of bAnmelden_ActionPerformed

   // end of btnRegistrieren_ActionPerformed

  // Ende Methoden
} // end of class Test
