package HeadBoyElection;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.event.*;

/**
  *
  * Beschreibung
  *
  * @version 1.0 vom 14.11.2018
  * @author 
  */

public class Fehlermeldung extends JFrame {
  // Anfang Attribute
  private JPanel jPanel1 = new JPanel(null, true);
    private JButton btnOK = new JButton();
    private JLabel lblFehler = new JLabel();
    private JLabel lDaseingegebenePasswortoder = new JLabel();
    private JLabel lBenutznameistfalsch = new JLabel();
  // Ende Attribute
  
  public Fehlermeldung() { 
    // Frame-Initialisierung
    super();
    setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
    int frameWidth = 220; 
    int frameHeight = 200;
    setSize(frameWidth, frameHeight);
    Dimension d = Toolkit.getDefaultToolkit().getScreenSize();
    int x = (d.width - getSize().width) / 2;
    int y = (d.height - getSize().height) / 2;
    setLocation(x, y);
    setTitle("Fehlermeldung");
    setResizable(false);
    Container cp = getContentPane();
    cp.setLayout(null);
    // Anfang Komponenten
    
    jPanel1.setBounds(0, 0, 220, 200);
    jPanel1.setOpaque(false);
    cp.add(jPanel1);
    btnOK.setBounds(72, 112, 75, 25);
    btnOK.setText("OK");
    btnOK.setMargin(new Insets(2, 2, 2, 2));
    btnOK.addActionListener(new ActionListener() { 
      public void actionPerformed(ActionEvent evt) { 
        btnOK_ActionPerformed(evt);
      }
    });
    jPanel1.add(btnOK);
    lblFehler.setBounds(80, 48, 110, 20);
    lblFehler.setText("Fehler");
    jPanel1.add(lblFehler);
    lDaseingegebenePasswortoder.setBounds(16, 64, 330, 20);
    lDaseingegebenePasswortoder.setText("Das eingegebene Passwort oder ");
    jPanel1.add(lDaseingegebenePasswortoder);
    lBenutznameistfalsch.setBounds(40, 80, 330, 20);
    lBenutznameistfalsch.setText("Benutzname ist falsch");
    jPanel1.add(lBenutznameistfalsch);
    // Ende Komponenten
    
    setVisible(true);
  } // end of public Fehlermeldung
  
  // Anfang Methoden
  
  public static void main(String[] args) {
    new Fehlermeldung();
  } // end of main
  
  public void btnOK_ActionPerformed(ActionEvent evt) {
    this.dispose();
    
  } // end of btnOK_ActionPerformed

  // Ende Methoden
} // end of class Fehlermeldung