package HeadBoyElection;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.event.*;

import headboyLogic.Admin;
import headboyLogic.Login;

/**
  *
  * Beschreibung
  *
  * @version 1.0 vom 06.11.2018
  * @author 
  */

public class Hauptmenue extends JFrame {
  // Anfang Attribute
  private JPanel jPanel1 = new JPanel(null, true);
    private JLabel lHauptmenu = new JLabel();
    private JButton bWahlen = new JButton();  
    private JButton bVorschlagen = new JButton();
    private JButton btnWahlergebnis = new JButton();
    private JButton bAbmelden = new JButton();
    private JButton btnWahlphase = new JButton();
    private JButton btnZurueck = new JButton();
    Admin admin = new Admin ();
    // Ende Attribute
  
  public Hauptmenue() { 
    // Frame-Initialisierung
    super();
    setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
    int frameWidth = 600; 
    int frameHeight = 400;
    setSize(frameWidth, frameHeight);
    Dimension d = Toolkit.getDefaultToolkit().getScreenSize();
    int x = (d.width - getSize().width) / 2;
    int y = (d.height - getSize().height) / 2;
    setLocation(x, y);
    setTitle("Hauptmen�");
    setResizable(false);
    Container cp = getContentPane();
    cp.setLayout(null);
    // Anfang Komponenten
    
    jPanel1.setBounds(0, 0, 600, 400);
    jPanel1.setOpaque(false);
    cp.add(jPanel1);
    lHauptmenu.setBounds(144, 8, 222, 36);
    lHauptmenu.setText("Hauptmen�");
    lHauptmenu.setHorizontalAlignment(SwingConstants.CENTER);
    jPanel1.add(lHauptmenu);
    if (admin.getWahlphase()==2) {
    	bWahlen.setBounds(112, 112, 163, 33);
        bWahlen.setText("W�hlen");
        bWahlen.setMargin(new Insets(2, 2, 2, 2));
        bWahlen.addActionListener(new ActionListener() { 
          public void actionPerformed(ActionEvent evt) { 
            bWahlen_ActionPerformed(evt);
          }
    });
    jPanel1.add(bWahlen);
    }
    if (admin.getWahlphase()==1) {
    bVorschlagen.setBounds(304, 112, 163, 33);
    bVorschlagen.setText("Vorschlagen");
    bVorschlagen.setMargin(new Insets(2, 2, 2, 2));
    bVorschlagen.addActionListener(new ActionListener() { 
      public void actionPerformed(ActionEvent evt) { 
        bVorschlagen_ActionPerformed(evt);
      }
    });
    jPanel1.add(bVorschlagen);
    }
    if(admin.getWahlphase()==3) {
    btnWahlergebnis.setBounds(112, 192, 163, 33);
    btnWahlergebnis.setText("Wahlergebnis");
    btnWahlergebnis.setMargin(new Insets(2, 2, 2, 2));
    btnWahlergebnis.addActionListener(new ActionListener() { 
      public void actionPerformed(ActionEvent evt) { 
        btnWahlergebnis_ActionPerformed(evt);
      }
    });
    jPanel1.add(btnWahlergebnis);
    }
    bAbmelden.setBounds(304, 192, 163, 33);
    bAbmelden.setText("Abmelden");
    bAbmelden.setMargin(new Insets(2, 2, 2, 2));
    bAbmelden.addActionListener(new ActionListener() { 
      public void actionPerformed(ActionEvent evt) { 
        bAbmelden_ActionPerformed(evt);
      }
    });
    jPanel1.add(bAbmelden);
    btnWahlphase.setBounds(216, 264, 163, 33);
    btnWahlphase.setText("Wahlphase");
    btnWahlphase.setMargin(new Insets(2, 2, 2, 2));
    btnWahlphase.addActionListener(new ActionListener() { 
      public void actionPerformed(ActionEvent evt) { 
        btnWahlphase_ActionPerformed(evt);
      }
    });

    jPanel1.add(btnZurueck);
    // Ende Komponenten
    
    setVisible(true);
  } // end of public Hauptmenue
  
  // Anfang Methoden
  
  public static void main(String[] args) {
    new Hauptmenue();
  } // end of main
  
  public void bWahlen_ActionPerformed(ActionEvent evt) {
	if (admin.getWahlphase()==2) {
		Waehlen waehlen = new Waehlen ();
	    this.dispose();
	}
    
  } // end of bWahlen_ActionPerformed

  public void bVorschlagen_ActionPerformed(ActionEvent evt) {
   if (admin.getWahlphase()==1) {
	   Vorschlagen vorschlagen = new Vorschlagen();
	   this.dispose();
   }
    
  } // end of bVorschlagen_ActionPerformed

  public void btnWahlergebnis_ActionPerformed(ActionEvent evt) {
    if (admin.getWahlphase()==3) {
    	Wahlergebnis wahlergebnis = new Wahlergebnis ();
        this.dispose();
    }
  } // end of btnWahlergebnis_ActionPerformed

  public void bAbmelden_ActionPerformed(ActionEvent evt) {
    Login login = new Login ();
    login.logOut();
    Anmeldung anmeldung = new Anmeldung ();
    this.dispose();
    
  } // end of bAbmelden_ActionPerformed

  public void btnWahlphase_ActionPerformed(ActionEvent evt) {
	 btnWahlphase.setText(getName()+admin.getWahlphase());
    
  } // end of btnWahlphase_ActionPerformed

 

  // Ende Methoden
} // end of class Hauptmenue
