package HeadBoyElection;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.event.*;

/**
  *
  * Beschreibung
  *
  * @version 1.0 vom 14.11.2018
  * @author 
  */

public class AlleStimmenabgegeben extends JFrame {
  // Anfang Attribute
  private JPanel jPanel1 = new JPanel(null, true);
    private JButton btnOK = new JButton();
    private JLabel lblSiehabenalleStimmenabgeben = new JLabel();
    private JLabel lblIhreWahlenwurdenabgespeichert = new JLabel();
    private JLabel lblUnddenWahlergebnissenhinzugefugt = new JLabel();
  // Ende Attribute
  
  public AlleStimmenabgegeben() { 
    // Frame-Initialisierung
    super();
    setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
    int frameWidth = 300; 
    int frameHeight = 200;
    setSize(frameWidth, frameHeight);
    Dimension d = Toolkit.getDefaultToolkit().getScreenSize();
    int x = (d.width - getSize().width) / 2;
    int y = (d.height - getSize().height) / 2;
    setLocation(x, y);
    setTitle("AlleStimmenabgegeben");
    setResizable(false);
    Container cp = getContentPane();
    cp.setLayout(null);
    // Anfang Komponenten
    
    jPanel1.setBounds(0, 0, 300, 200);
    jPanel1.setOpaque(false);
    cp.add(jPanel1);
    btnOK.setBounds(112, 112, 75, 25);
    btnOK.setText("OK");
    btnOK.setMargin(new Insets(2, 2, 2, 2));
    btnOK.addActionListener(new ActionListener() { 
      public void actionPerformed(ActionEvent evt) { 
        btnOK_ActionPerformed(evt);
      }
    });
    jPanel1.add(btnOK);
    lblSiehabenalleStimmenabgeben.setBounds(40, 32, 192, 20);
    lblSiehabenalleStimmenabgeben.setText("Sie haben alle Stimmen abgeben.");
    jPanel1.add(lblSiehabenalleStimmenabgeben);
    lblIhreWahlenwurdenabgespeichert.setBounds(40, 48, 227, 20);
    lblIhreWahlenwurdenabgespeichert.setText("Ihre Wahlen wurden abgespeichert ");
    jPanel1.add(lblIhreWahlenwurdenabgespeichert);
    lblUnddenWahlergebnissenhinzugefugt.setBounds(40, 64, 224, 20);
    lblUnddenWahlergebnissenhinzugefugt.setText("und den Wahlergebnissen hinzugefügt.");
    jPanel1.add(lblUnddenWahlergebnissenhinzugefugt);
    // Ende Komponenten
    
    setVisible(true);
  } // end of public AlleStimmenabgegeben
  
  // Anfang Methoden
  
  public static void main(String[] args) {
    new AlleStimmenabgegeben();
  } // end of main
  
  public void btnOK_ActionPerformed(ActionEvent evt) {
    Hauptmenue hauptmenue = new Hauptmenue ();
    this.dispose();
    
  } // end of btnOK_ActionPerformed

  // Ende Methoden
} // end of class AlleStimmenabgegeben
