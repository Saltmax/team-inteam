package HeadBoyElection;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.event.*;

/**
  *
  * Beschreibung
  *
  * @version 1.0 vom 06.11.2018
  * @author 
  */

public class Sindsiesicher extends JFrame {
  // Anfang Attribute
  private JPanel jPanel1 = new JPanel(null, true);
    private JButton btnJa = new JButton();
    private JButton btnNein = new JButton();
    private JLabel lblSindSiesicher = new JLabel();
  // Ende Attribute
  
  public Sindsiesicher() { 
    // Frame-Initialisierung
    super();
    setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
    int frameWidth = 300; 
    int frameHeight = 200;
    setSize(frameWidth, frameHeight);
    Dimension d = Toolkit.getDefaultToolkit().getScreenSize();
    int x = (d.width - getSize().width) / 2;
    int y = (d.height - getSize().height) / 2;
    setLocation(x, y);
    setTitle("Sindsiesicher");
    setResizable(false);
    Container cp = getContentPane();
    cp.setLayout(null);
    // Anfang Komponenten
    
    jPanel1.setBounds(0, -8, 300, 200);
    jPanel1.setOpaque(false);
    cp.add(jPanel1);
    btnJa.setBounds(72, 104, 75, 25);
    btnJa.setText("Ja");
    btnJa.setMargin(new Insets(2, 2, 2, 2));
    btnJa.addActionListener(new ActionListener() { 
      public void actionPerformed(ActionEvent evt) { 
        btnJa_ActionPerformed(evt);
      }
    });
    jPanel1.add(btnJa);
    btnNein.setBounds(176, 104, 75, 25);
    btnNein.setText("Nein");
    btnNein.setMargin(new Insets(2, 2, 2, 2));
    btnNein.addActionListener(new ActionListener() { 
      public void actionPerformed(ActionEvent evt) { 
        btnNein_ActionPerformed(evt);
      }
    });
    jPanel1.add(btnNein);
    lblSindSiesicher.setBounds(112, 56, 110, 20);
    lblSindSiesicher.setText("Sind Sie sicher ?");
    jPanel1.add(lblSindSiesicher);
    // Ende Komponenten
    
    setVisible(true);
  } // end of public Sindsiesicher
  
  // Anfang Methoden
  
  public static void main(String[] args) {
    new Sindsiesicher();
  } // end of main
  
  public void btnJa_ActionPerformed(ActionEvent evt) {
    this.dispose();
    
  } // end of btnJa_ActionPerformed

  public void btnNein_ActionPerformed(ActionEvent evt) {
	  Startbildschirm startbildschirm = new Startbildschirm();
	  this.dispose();
    
  } // end of btnNein_ActionPerformed

  // Ende Methoden
} // end of class Sindsiesicher