package HeadBoyElection;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.event.*;

/**
  *
  * Beschreibung
  *
  * @version 1.0 vom 14.11.2018
  * @author 
  */

public class UserstehtbereitszurWahl extends JFrame {
  // Anfang Attribute
  private JPanel jPanel1 = new JPanel(null, true);
    private JButton btnOK = new JButton();
    private JLabel lblDieserUserstehtbereitszurWahl = new JLabel();
  // Ende Attribute
  
  public UserstehtbereitszurWahl() { 
    // Frame-Initialisierung
    super();
    setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
    int frameWidth = 300; 
    int frameHeight = 200;
    setSize(frameWidth, frameHeight);
    Dimension d = Toolkit.getDefaultToolkit().getScreenSize();
    int x = (d.width - getSize().width) / 2;
    int y = (d.height - getSize().height) / 2;
    setLocation(x, y);
    setTitle("UserstehbereitszurWahl");
    setResizable(false);
    Container cp = getContentPane();
    cp.setLayout(null);
    // Anfang Komponenten
    
    jPanel1.setBounds(0, 0, 300, 200);
    jPanel1.setOpaque(false);
    cp.add(jPanel1);
    btnOK.setBounds(112, 96, 75, 25);
    btnOK.setText("OK");
    btnOK.setMargin(new Insets(2, 2, 2, 2));
    btnOK.addActionListener(new ActionListener() { 
      public void actionPerformed(ActionEvent evt) { 
        btnOK_ActionPerformed(evt);
      }
    });
    jPanel1.add(btnOK);
    lblDieserUserstehtbereitszurWahl.setBounds(48, 64, 202, 20);
    lblDieserUserstehtbereitszurWahl.setText("Dieser User steht bereits zur Wahl ");
    jPanel1.add(lblDieserUserstehtbereitszurWahl);
    // Ende Komponenten
    
    setVisible(true);
  } // end of public UserstehbereitszurWahl
  
  // Anfang Methoden
  
  public static void main(String[] args) {
    new UserstehtbereitszurWahl();
  } // end of main
  
  public void btnOK_ActionPerformed(ActionEvent evt) {
    this.dispose();
    
  } // end of btnOK_ActionPerformed

  // Ende Methoden
} // end of class UserstehbereitszurWahl
