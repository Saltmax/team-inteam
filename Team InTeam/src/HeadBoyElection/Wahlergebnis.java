package HeadBoyElection;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.event.*;

import headboyLogic.Login;

/**
  *
  * Beschreibung
  *
  * @version 1.0 vom 20.11.2018
  * @author 
  */

public class Wahlergebnis extends JFrame {
  // Anfang Attribute
  private JPanel jPanel1 = new JPanel(null, true);
    private JButton btnZuruck = new JButton();
    private JLabel lblWahlergebnis = new JLabel();
    private JLabel lblKandidaten = new JLabel();
    private JLabel lblStimmen = new JLabel();
    private JLabel l4 = new JLabel();
    private JLabel l5 = new JLabel();
    private JLabel l6 = new JLabel();
    private JLabel l7 = new JLabel();
    private JLabel l3 = new JLabel();
    private JLabel l2 = new JLabel();
    private JLabel l11 = new JLabel();
    private JLabel l8 = new JLabel();
    private JLabel l9 = new JLabel();
    private JLabel l10 = new JLabel();
    private JLabel l1 = new JLabel();
    private JLabel l12 = new JLabel();
    private JLabel l13 = new JLabel();
    private JLabel l14 = new JLabel();
    private JLabel l15 = new JLabel();
    private JLabel l16 = new JLabel();
    private JLabel l17 = new JLabel();
    private JLabel l18 = new JLabel();
    private JLabel l19 = new JLabel();
    private JLabel l20 = new JLabel();
    private JLabel l21 = new JLabel();
    private JLabel l22 = new JLabel();
    private JLabel l23 = new JLabel();
    private JLabel l24 = new JLabel();
  // Ende Attribute
  
  public Wahlergebnis() { 
    // Frame-Initialisierung
    super();
    setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
    int frameWidth = 600; 
    int frameHeight = 400;
    setSize(frameWidth, frameHeight);
    Dimension d = Toolkit.getDefaultToolkit().getScreenSize();
    int x = (d.width - getSize().width) / 2;
    int y = (d.height - getSize().height) / 2;
    setLocation(x, y);
    setTitle("Wahlergebnis");
    setResizable(false);
    Container cp = getContentPane();
    cp.setLayout(null);
    // Anfang Komponenten
    
    jPanel1.setBounds(0, 0, 600, 400);
    jPanel1.setOpaque(false);
    cp.add(jPanel1);
    btnZuruck.setBounds(24, 24, 75, 25);
    btnZuruck.setText("Zur�ck");
    btnZuruck.setMargin(new Insets(2, 2, 2, 2));
    btnZuruck.addActionListener(new ActionListener() { 
      public void actionPerformed(ActionEvent evt) { 
        actionPerformed(evt);
      }
    });
    jPanel1.add(btnZuruck);
    lblWahlergebnis.setBounds(240, 24, 110, 20);
    lblWahlergebnis.setText("Wahlergebnis");
    jPanel1.add(lblWahlergebnis);
    lblKandidaten.setBounds(40, 88, 110, 20);
    lblKandidaten.setText("Kandidaten");
    jPanel1.add(lblKandidaten);
    lblStimmen.setBounds(440, 88, 110, 20);
    lblStimmen.setText("Stimmen");
    jPanel1.add(lblStimmen);
    l4.setBounds(40, 152, 110, 20);
    l4.setText("4.");
    jPanel1.add(l4);
    l5.setBounds(40, 168, 110, 20);
    l5.setText("5.");
    jPanel1.add(l5);
    l6.setBounds(40, 184, 110, 20);
    l6.setText("6.");
    jPanel1.add(l6);
    l7.setBounds(40, 200, 110, 20);
    l7.setText("7.");
    jPanel1.add(l7);
    l3.setBounds(40, 136, 110, 20);
    l3.setText("3.");
    jPanel1.add(l3);
    l2.setBounds(40, 120, 110, 20);
    l2.setText("2.");
    jPanel1.add(l2);
    l11.setBounds(40, 104, 110, 20);
    l11.setText("1.");
    jPanel1.add(l11);
    l8.setBounds(40, 216, 110, 20);
    l8.setText("8.");
    jPanel1.add(l8);
    l9.setBounds(40, 232, 110, 20);
    l9.setText("9.");
    jPanel1.add(l9);
    l10.setBounds(40, 248, 110, 20);
    l10.setText("10.");
    jPanel1.add(l10);
    l1.setBounds(40, 264, 110, 20);
    l1.setText("11.");
    jPanel1.add(l1);
    l12.setBounds(40, 280, 110, 20);
    l12.setText("12.");
    jPanel1.add(l12);
    l13.setBounds(440, 104, 110, 20);
    l13.setText("13.");
    jPanel1.add(l13);
    l14.setBounds(440, 120, 110, 20);
    l14.setText("14.");
    jPanel1.add(l14);
    l15.setBounds(440, 136, 110, 20);
    l15.setText("15.");
    jPanel1.add(l15);
    l16.setBounds(440, 152, 110, 20);
    l16.setText("16.");
    jPanel1.add(l16);
    l17.setBounds(440, 168, 110, 20);
    l17.setText("17.");
    jPanel1.add(l17);
    l18.setBounds(440, 184, 110, 20);
    l18.setText("18.");
    jPanel1.add(l18);
    l19.setBounds(440, 200, 110, 20);
    l19.setText("19.");
    jPanel1.add(l19);
    l20.setBounds(440, 216, 110, 20);
    l20.setText("20.");
    jPanel1.add(l20);
    l21.setBounds(440, 232, 110, 20);
    l21.setText("21.");
    jPanel1.add(l21);
    l22.setBounds(440, 248, 110, 20);
    l22.setText("22.");
    jPanel1.add(l22);
    l23.setBounds(440, 264, 110, 20);
    l23.setText("23.");
    jPanel1.add(l23);
    l24.setBounds(440, 280, 110, 20);
    l24.setText("24.");
    jPanel1.add(l24);
    // Ende Komponenten
    
    setVisible(true);
  } // end of public Wahlergebnis

  
  // Anfang Methoden
  
  public static void main(String[] args) {
    new Wahlergebnis();
  } // end of main
  
  public void btnZurueck_ActionPerformed(ActionEvent evt) {
    Login login = new Login ();
	  if (login.UserIsAdmin(login.getAktuellerUser())) {
		  AdministratorFenster adminfenster = new AdministratorFenster();
		  
	  }else {
		  Hauptmenue hauptmenue = new Hauptmenue ();
	  }
	  this.dispose();
    
  } // end of btnZurueck_ActionPerformed

  // Ende Methoden
} // end of class Wahlergebnis
