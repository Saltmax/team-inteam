package HeadBoyElection;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.event.*;

import headboyLogic.Admin;

/**
  *
  * Beschreibung
  *
  * @version 1.0 vom 06.11.2018
  * @author 
  */

public class AdministratorFenster extends JFrame {
  // Anfang Attribute
  private JPanel jPanel1 = new JPanel(null, true);
    private JButton btnZuruck = new JButton();
    private JButton btnSetAnzahlStimmen = new JButton();
    private JButton btnAktuellesWahlergebnis = new JButton();
    private JButton btnPhaseanzeigen = new JButton();
    private JLabel lblAdministratorFenster = new JLabel();
    private JTextField txtAnzahl = new JTextField();
    private JButton btnRegistrieren = new JButton();
    Admin admin = new Admin();
    // Ende Attribute
  
  public AdministratorFenster() { 
    // Frame-Initialisierung
    super();
    setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
    int frameWidth = 600; 
    int frameHeight = 400;
    setSize(frameWidth, frameHeight);
    Dimension d = Toolkit.getDefaultToolkit().getScreenSize();
    int x = (d.width - getSize().width) / 2;
    int y = (d.height - getSize().height) / 2;
    setLocation(x, y);
    setTitle("AdministratorFenster");
    setResizable(false);
    Container cp = getContentPane();
    cp.setLayout(null);
    // Anfang Komponenten
    
    jPanel1.setBounds(0, 0, 600, 400);
    jPanel1.setOpaque(false);
    cp.add(jPanel1);
    btnZuruck.setBounds(24, 32, 75, 25);
    btnZuruck.setText("Zur�ck");
    btnZuruck.setMargin(new Insets(2, 2, 2, 2));
    btnZuruck.addActionListener(new ActionListener() { 
      public void actionPerformed(ActionEvent evt) { 
        btnZuruck_ActionPerformed(evt);
      }
    });
    jPanel1.add(btnZuruck);
    btnSetAnzahlStimmen.setBounds(96, 104, 163, 33);
    btnSetAnzahlStimmen.setText("set Anzahl Stimmen");
    btnSetAnzahlStimmen.setMargin(new Insets(2, 2, 2, 2));
    btnSetAnzahlStimmen.addActionListener(new ActionListener() { 
      public void actionPerformed(ActionEvent evt) { 
        btnSetAnzahlStimmen_ActionPerformed(evt);
      }
    });
    jPanel1.add(btnSetAnzahlStimmen);
    btnAktuellesWahlergebnis.setBounds(96, 144, 163, 33);
    btnAktuellesWahlergebnis.setText("Aktuelles Wahlergebnis");
    btnAktuellesWahlergebnis.setMargin(new Insets(2, 2, 2, 2));
    btnAktuellesWahlergebnis.addActionListener(new ActionListener() { 
      public void actionPerformed(ActionEvent evt) { 
        btnAktuellesWahlergebnis_ActionPerformed(evt);
      }
    });
    jPanel1.add(btnAktuellesWahlergebnis);
    btnPhaseanzeigen.setBounds(96, 184, 163, 33);
    btnPhaseanzeigen.setText("Phase anzeigen");
    btnPhaseanzeigen.setMargin(new Insets(2, 2, 2, 2));
    btnPhaseanzeigen.addActionListener(new ActionListener() { 
      public void actionPerformed(ActionEvent evt) { 
        btnPhaseanzeigen_ActionPerformed(evt);
      }
    });
    jPanel1.add(btnPhaseanzeigen);
    lblAdministratorFenster.setBounds(224, 32, 152, 28);
    lblAdministratorFenster.setText("Administrator Fenster");
    jPanel1.add(lblAdministratorFenster);
    txtAnzahl.setBounds(280, 104, 38, 36);
    jPanel1.add(txtAnzahl);
    
 
	btnRegistrieren.setBounds(96, 222, 163, 33);
    btnRegistrieren.setText("Registrieren");
    btnRegistrieren.setMargin(new Insets(2, 2, 2, 2));
    btnRegistrieren.addActionListener(new ActionListener() { 
      public void actionPerformed(ActionEvent evt) { 
        btnRegistrieren_ActionPerformend(evt);
      }
    });
    jPanel1.add(btnRegistrieren);
    
    // Ende Komponenten
    
    setVisible(true);
  } // end of public AdministratorFenster
  
  // Anfang Methoden
  
  public static void main(String[] args) {
    new AdministratorFenster();
  } // end of main
  
  public void btnZuruck_ActionPerformed(ActionEvent evt) {
    Anmeldung anmeldung = new Anmeldung ();
    this.dispose();
    
  } // end of btnZuruck_ActionPerformed

  public void btnSetAnzahlStimmen_ActionPerformed(ActionEvent evt) {
    admin.setMaxStimmen(txtAnzahl.getText());
    txtAnzahl.setText("");
  } // end of btnSetAnzahlStimmen_ActionPerformed

  public void btnAktuellesWahlergebnis_ActionPerformed(ActionEvent evt) {
    Wahlergebnis wahlergebnis = new Wahlergebnis ();
    this.dispose();
    
  } // end of btnAktuellesWahlergebnis_ActionPerformed

  public void btnPhaseanzeigen_ActionPerformed(ActionEvent evt) {
    Admin admin = new Admin ();
    admin.setWahlphase(txtAnzahl.getText());
    txtAnzahl.setText("");
  } // end of btnPhaseanzeigen_ActionPerformed

  public void btnRegistrieren_ActionPerformend(ActionEvent evt) {
	  Registrierung registrierung = new Registrierung();
	  this.dispose();
  }
  // Ende Methoden
} // end of class AdministratorFenster
