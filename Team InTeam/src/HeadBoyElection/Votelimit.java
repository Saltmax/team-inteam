package HeadBoyElection;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.event.*;

/**
  *
  * Beschreibung
  *
  * @version 1.0 vom 06.11.2018
  * @author 
  */

public class Votelimit extends JFrame {
  // Anfang Attribute
  private JPanel jPanel1 = new JPanel(null, true);
    private JButton jButton1 = new JButton();
    private JButton jButton2 = new JButton();
    private JButton btnOK = new JButton();
    private JLabel lblSiehabenbereitsihrVotelimiterreicht = new JLabel();
  // Ende Attribute
  
  public Votelimit() { 
    // Frame-Initialisierung
    super();
    setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
    int frameWidth = 300; 
    int frameHeight = 200;
    setSize(frameWidth, frameHeight);
    Dimension d = Toolkit.getDefaultToolkit().getScreenSize();
    int x = (d.width - getSize().width) / 2;
    int y = (d.height - getSize().height) / 2;
    setLocation(x, y);
    setTitle("VoteLimit");
    setResizable(false);
    Container cp = getContentPane();
    cp.setLayout(null);
    // Anfang Komponenten
    
    jPanel1.setBounds(0, 0, 300, 200);
    jPanel1.setOpaque(false);
    cp.add(jPanel1);
    jButton1.setBounds(104, 264, 75, 25);
    jButton1.setText("jButton1");
    jButton1.setMargin(new Insets(2, 2, 2, 2));
    jButton1.addActionListener(new ActionListener() { 
      public void actionPerformed(ActionEvent evt) { 
        jButton1_ActionPerformed(evt);
      }
    });
    jPanel1.add(jButton1);
    jButton2.setBounds(344, 296, 75, 25);
    jButton2.setText("jButton2");
    jButton2.setMargin(new Insets(2, 2, 2, 2));
    jButton2.addActionListener(new ActionListener() { 
      public void actionPerformed(ActionEvent evt) { 
        jButton2_ActionPerformed(evt);
      }
    });
    jPanel1.add(jButton2);
    btnOK.setBounds(112, 96, 75, 25);
    btnOK.setText("OK ");
    btnOK.setMargin(new Insets(2, 2, 2, 2));
    btnOK.addActionListener(new ActionListener() { 
      public void actionPerformed(ActionEvent evt) { 
        btnOK_ActionPerformed(evt);
      }
    });
    jPanel1.add(btnOK);
    lblSiehabenbereitsihrVotelimiterreicht.setBounds(32, 40, 225, 36);
    lblSiehabenbereitsihrVotelimiterreicht.setText("Sie haben bereits ihr Votelimit erreicht ");
    jPanel1.add(lblSiehabenbereitsihrVotelimiterreicht);
    // Ende Komponenten
    
    setVisible(true);
  } // end of public VoteLimit
  
  // Anfang Methoden
  
  public static void main(String[] args) {
    new Votelimit();
  } // end of main
  
  public void jButton1_ActionPerformed(ActionEvent evt) {
    // TODO hier Quelltext einf�gen
    
  } // end of jButton1_ActionPerformed

  public void jButton2_ActionPerformed(ActionEvent evt) {
    // TODO hier Quelltext einf�gen
    
  } // end of jButton2_ActionPerformed

  public void btnOK_ActionPerformed(ActionEvent evt) {
    // TODO hier Quelltext einf�gen
    
  } // end of btnOK_ActionPerformed

  // Ende Methoden
} // end of class VoteLimit
