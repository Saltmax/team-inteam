package headboyLogic;

import HeadBoyDBSchicht.Datenbankschicht;

public class CreateNewKandidat {
Datenbankschicht dB = new Datenbankschicht ();
	
	public CreateNewKandidat() {
		
	}

	public void createNewKandidat(Schueler dude) {
		
		Kandidat kandidat = new Kandidat();
	kandidat.setVorname(dude.getVorname());
	kandidat.setNachname(dude.getNachname());
	kandidat.setPasswort(dude.getPasswort());
	kandidat.setUserID(dude.getUserID());
	kandidat.setVerbleibendeStimmen(dB.getMaxAnzahlStimmen());
	dB.schuelerLoeschen(dude);
	dB.personEintragen(kandidat.getVorname(),kandidat.getNachname(),kandidat.getPasswort(),kandidat.getUserID());
	dB.kandidatEintragen(kandidat);
	}
	
}
