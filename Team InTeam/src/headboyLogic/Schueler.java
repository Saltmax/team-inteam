package headboyLogic;
import java.util.ArrayList;
import HeadBoyDBSchicht.Datenbankschicht;
import HeadBoyElection.AlleStimmenabgegeben;
import HeadBoyElection.UserstehtbereitszurWahl;

public class Schueler extends Person {
String jahrgang;

int verbleibendeStimmen;
boolean vorgeschlagen;
Datenbankschicht dB = new Datenbankschicht();
CreateNewKandidat createNewKandidat = new CreateNewKandidat();
	
	public Schueler () {
		
	}
	public Schueler(String vorname, String nachname, String passwort, int userID, String jahrgang, int verbleibendeStimmen, boolean vorgeschlagen) {
	this.vorname = vorname;
	this.nachname = nachname;
	this.passwort = passwort;
	this.userID = userID;
	this.jahrgang = jahrgang;
	this.vorgeschlagen = vorgeschlagen;
	this.verbleibendeStimmen = verbleibendeStimmen;
	}

	public void waehlen(String vorname,String nachname, String jahrgang) {
		if (this.verbleibendeStimmen != 0) {
			this.verbleibendeStimmen = this.verbleibendeStimmen-1;
			dB.erhoeheStimmenErhalten(vorname,nachname,jahrgang);
			
		}else {
			AlleStimmenabgegeben alleStimmenAbgegeben = new AlleStimmenabgegeben ();
		}
	}
	
	public void vorschlagen (String vorname, String nachname,String jahrgang) {
		dB.setSchuelerVorgeschlagen(vorname,nachname,jahrgang);
	}

	public ArrayList wahlergebnisAnzeigen() {
		return dB.WahlergebnisAusgeben();
	}
	
	public String getJahrgang() {
		return jahrgang;
	}

	public void setJahrgang(String jahrgang) {
		this.jahrgang = jahrgang;
	}

	public int getVerbleibendeStimmen() {
		return verbleibendeStimmen;
	}

	public void setVerbleibendeStimmen(int verbleibendeStimmen) {
		this.verbleibendeStimmen = verbleibendeStimmen;
	}
	public Schueler getSchueler(int aktuellerUser) {
		
		Datenbankschicht dB = new Datenbankschicht ();
		ArrayList arrayliste = new ArrayList ();
		arrayliste = dB.getSchuelerVonID(aktuellerUser);
		Schueler schueler = new Schueler ();
		schueler.setVorname(String.valueOf(arrayliste.get(1)));
		schueler.setNachname(String.valueOf(arrayliste.get(2)));
		schueler.setPasswort(String.valueOf(arrayliste.get(3)));
		schueler.setUserID(aktuellerUser);
		schueler.setJahrgang(String.valueOf(arrayliste.get(4)));
		schueler.setVerbleibendeStimmen(Integer.parseInt((String) arrayliste.get(5)));
		return schueler;
		// TODO Auto-generated method stub
		
	}
	public void setVorgeschlagen(boolean vorgeschlagen) {
	Login login = new Login ();
		dB.setSchuelerVorgeschlagen(login.getAktuellerUser(),false);
		
	}


}
