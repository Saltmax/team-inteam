package headboyLogic;

public abstract class Person {
String vorname;
String nachname;
String passwort;
int userID;


public String getVorname() {
	return vorname;
}
public void setVorname(String object) {
	this.vorname = object;
}
public String getNachname() {
	return nachname;
}
public void setNachname(String nachname) {
	this.nachname = nachname;
}
public String getPasswort() {
	return passwort;
}
public void setPasswort(String passwort) {
	this.passwort = passwort;
}
public int getUserID() {
	return userID;
}
public void setUserID(int userID) {
	this.userID = userID;
}
}
