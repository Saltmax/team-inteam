package headboyLogic;
import java.util.ArrayList;
import HeadBoyDBSchicht.Datenbankschicht;

public class Admin extends Person{

	Datenbankschicht dB = new Datenbankschicht();
	int Wahlphase;
	int maxStimmen;
	public Admin () {
		
	}
	public Admin(String vorname, String nachname, String passwort, int userID, int wahlphase) {	
		this.vorname = vorname;
		this.nachname = nachname;
		this.passwort = passwort;
		this.userID = userID;
		this.Wahlphase = wahlphase;
	}	
	
	public void setMaxStimmen(String string) {
		
		dB.setMaxStimmen(string);
	}

	public ArrayList WahlergebnisAusgeben() {
		return dB.WahlergebnisAusgeben();
	} 
	public void setWahlphase (String zahl) {
		dB.setWahlphase(zahl);
	}
	public int getWahlphase () {
		return dB.getWahlphase();
	}

	public String getVorname() {
		return this.vorname;
	}

	public String getNachname() {
		return this.nachname;
	}
	
	public String getPasswort() {
		return this.passwort;
	}

	public int getUserID() {
		return this.userID;
	}
	public int getMaxStimmen() {
		// TODO Auto-generated method stub
		return this.maxStimmen;
	}
}

