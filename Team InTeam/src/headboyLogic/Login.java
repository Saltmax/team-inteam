package headboyLogic;

import java.util.ArrayList;

import HeadBoyDBSchicht.Datenbankschicht;
import HeadBoyElection.AdministratorFenster;
import HeadBoyElection.Anmeldung;
import HeadBoyElection.Fehlermeldung;
import HeadBoyElection.Hauptmenue;
import HeadBoyElection.Wahlergebnis;
import HeadBoyElection.ZurWahlvorgeschlagen;

public class Login {
	int aktuelleUserID;
	Datenbankschicht dB = new Datenbankschicht();
	Logic logic = new Logic ();
	public Login () {
		}

	public void logOut() {
		this.aktuelleUserID = 0;
	}
	
	public void logIn(String Benutzername, String passwort) { 
		if (dB.istKandidatInDatabase(Benutzername,passwort)) {
			setAktuellerUser(dB.getKandidatID(Benutzername,passwort));
			if (dB.getWahlphase()==3) {
				Wahlergebnis wahlergebnis = new Wahlergebnis ();
			}else {
				Hauptmenue hauptmenue = new Hauptmenue ();
				ZurWahlvorgeschlagen vorgeschlagen = new ZurWahlvorgeschlagen();
			}
			
		}else {
			if (dB.istSchuelerInDatabase(Benutzername, passwort)) {
				setAktuellerUser(dB.getSchuelerID(Benutzername, passwort));
				if (dB.getWahlphase()==3) {
					Wahlergebnis wahlergebnis = new Wahlergebnis ();
				}else {
					if (dB.getSchuelerVorgeschlagen(Benutzername, passwort)) {
						ZurWahlvorgeschlagen zwv = new ZurWahlvorgeschlagen ();
						Hauptmenue hauptmenue = new Hauptmenue ();
					}else {
						Hauptmenue hauptmenue = new Hauptmenue ();
					}
					
					  }
				}else {
					if (dB.istAdminDude(Benutzername,passwort)) {
						AdministratorFenster adminFenster = new AdministratorFenster();
					}else {
						Fehlermeldung fehlermeldung = new Fehlermeldung();
						Anmeldung anmeldung = new Anmeldung();
					}
				}
		}
		
	}

	public int getAktuellerUser() {
		return this.aktuelleUserID;
	}

	private void setAktuellerUser(int userID) {
		this.aktuelleUserID = userID;
	}
	public boolean UserIsAdmin (int userID) {
		return dB.istAdmin(userID);
	}
}